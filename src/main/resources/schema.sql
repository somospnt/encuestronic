-- Este archivo es esjecutado automátciamente por Spring Boot, de manera que
-- cuando se ejecutan los test se crea una base de datos en memoria (HSQLDB) y
-- se ejecuta esta script contra dicha base.
-- Al correr los tests, se cuenta con una base con datos de prueba listos para
-- ser usados. Cuando termina la ejecución de las pruebas la base de datos deja
-- de existir y todos los datos se pierden.

DROP TABLE IF EXISTS entrevista;


CREATE TABLE entrevista (
    id BIGINT IDENTITY PRIMARY KEY,
    entrevistado VARCHAR(45),
    entrevistador VARCHAR(45),
    descripcion VARCHAR(100),
    fecha DATETIME,
    realizada BOOLEAN
);

INSERT INTO entrevista VALUES (1, 'Juan Ortiz', 'Rober', '5/4 a las 17:30hs.','2018-04-05 17:30:00', true);
INSERT INTO entrevista VALUES (2, 'Lalo Lola', 'Rober', '7/4 a las 7:30hs.','2018-04-07 07:30:00', false);
INSERT INTO entrevista VALUES (3, 'Juan Ortiz', 'Pablo', '5/4 a las 17:30hs.','2018-04-05 17:30:00', true);
INSERT INTO entrevista VALUES (4, 'Lalo Lola', 'Pablo', '7/4 a las 7:30hs.','2018-04-07 07:30:00', false);
