package com.connectis.encuestronic.service;

import com.connectis.encuestronic.domain.Entrevista;
import com.connectis.encuestronic.repository.EntrevistaRepository;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Transactional
@Service
public class EntrevistaService {

    private final String ENTREVISTA_NULA = "La entrevista es nula.";
    private final String CAMPO_NULO = "Uno de los campos nulos no puede ser nulo";
    private final String CAMPO_VACIO = "Uno de los campos vacíos no puede estar vacío";
    private final String ID_EXISTENTE = "Ya existe una entrevista con ese ID.";
    private final String ID_NULO = "El id es nulo.";
    private final String ID_FUERA_DE_RANGO = "El id está fuera de rango.";
    private final String NOMBRE_VACIO = "El nombre está vacio.";

    private final EntrevistaRepository entrevistaRepository;

    public EntrevistaService(EntrevistaRepository entrevistaRepository) {
        this.entrevistaRepository = entrevistaRepository;
    }

    public List<Entrevista> buscarTodas() {
        return entrevistaRepository.findAllByOrderByFechaAsc();
    }

    public Entrevista modificarEntrevista(Entrevista entrevista) {
        Assert.isTrue(entrevista != null, ENTREVISTA_NULA);
        Assert.isTrue(entrevista.getId() != null, ID_NULO);
        Entrevista entrevistaBase = entrevistaRepository.findById(entrevista.getId()).orElseThrow(() -> new IllegalArgumentException(ID_FUERA_DE_RANGO));
        Assert.isTrue(entrevista.getEntrevistado() != null, CAMPO_NULO);
        Assert.isTrue(!entrevista.getEntrevistado().equals(""), CAMPO_VACIO);
        Assert.isTrue(entrevista.getEntrevistador() != null, CAMPO_NULO);
        Assert.isTrue(!entrevista.getEntrevistador().equals(""), CAMPO_VACIO);

        entrevistaBase.setDescripcion(entrevista.getDescripcion());
        entrevistaBase.setEntrevistado(entrevista.getEntrevistado());
        entrevistaBase.setEntrevistador(entrevista.getEntrevistador());
        entrevistaBase.setFecha(entrevista.getFecha());
        entrevistaBase.setRealizada(entrevista.getRealizada());

        return entrevistaBase;
    }

    public Entrevista crearEntrevista(Entrevista entrevista) {
        Assert.isTrue(entrevista != null, ENTREVISTA_NULA);
        Assert.isNull(entrevista.getId(), ID_EXISTENTE);
        Assert.isTrue(!entrevista.getEntrevistado().trim().isEmpty(), NOMBRE_VACIO);
        return entrevistaRepository.save(entrevista);
    }

    public void borrarEntrevista(Entrevista entrevista) {
        Assert.isTrue(entrevista != null, ENTREVISTA_NULA);
        Assert.isTrue(entrevista.getId() != null, ID_NULO);
        entrevistaRepository.findById(entrevista.getId()).orElseThrow(() -> new IllegalArgumentException(ID_FUERA_DE_RANGO));
        entrevistaRepository.delete(entrevista);
    }

}
