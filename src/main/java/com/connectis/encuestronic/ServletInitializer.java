package com.connectis.encuestronic;

import com.connectis.encuestronic.EntrevistasApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 *
 * Esta clase es necesaria para poder desplegar la aplicación como war en un
 * contenedor de aplicaciones (en nuestro caso, Apache Tomcat).
 */
public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(EntrevistasApplication.class);
    }

}