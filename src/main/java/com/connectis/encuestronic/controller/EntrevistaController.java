package com.connectis.encuestronic.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class EntrevistaController {

    @GetMapping({"/entrevistas", "/"})
    public String entrevistas(Model model) {
        return "entrevistas";
    }

}
