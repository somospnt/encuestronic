package com.connectis.encuestronic.controller.rest;

import com.connectis.encuestronic.domain.Entrevista;
import com.connectis.encuestronic.service.EntrevistaService;
import java.util.List;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/entrevistas")
public class EntrevistaRestController {

    private final String ENTREVISTA_NO_COINCIDE = "No coincide el ID del objeto con la URL suministrada.";

    private final EntrevistaService entrevistaService;

    public EntrevistaRestController(EntrevistaService entrevistaService) {
        this.entrevistaService = entrevistaService;
    }

    @PutMapping("/{id}")
    public Entrevista modificarEntrevista(@PathVariable Long id, @RequestBody Entrevista entrevista) {
        Assert.isTrue(id == entrevista.getId(), ENTREVISTA_NO_COINCIDE);
        return entrevistaService.modificarEntrevista(entrevista);
    }

    @PostMapping()
    public Entrevista crearEntrevista(@RequestBody Entrevista entrevista) {
        return entrevistaService.crearEntrevista(entrevista);
    }

    @GetMapping()
    public List<Entrevista> buscarTodas() {
        return entrevistaService.buscarTodas();
    }

    @DeleteMapping("/{id}")
    public void borrarEntrevista(@PathVariable Long id, @RequestBody Entrevista entrevista) {
        Assert.isTrue(id == entrevista.getId(), ENTREVISTA_NO_COINCIDE);
        entrevistaService.borrarEntrevista(entrevista);
    }

}
