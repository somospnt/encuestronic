<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <meta charset="utf-8">
        <title>Encuestronic</title>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="vendor/mdb/css/bootstrap.min.css">
        <!-- Toastr JS -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="vendor/mdb/css/mdb.min.css" rel="stylesheet">
        <!--tempus dominus date picker-->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha18/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" type="text/css"/>
        <!-- Your custom styles (optional) -->
        <link href="css/style.css" rel="stylesheet">
    </head>

    <body>

        <!-- Navbar -->
        <nav class="navbar fixed-top navbar-dark top-nav-collapse pnt-hidden pnt-js-navbar">
            <div class="fluid-container justify-content-center align-items-center text-center white-text">
                <h3>Encuestronic</h3>
            </div>
        </nav>
        <!-- Navbar -->

        <!-- Full Page Intro -->
        <div class="view pnt-js-intro">
            <div class="mask teal darken-2 d-flex justify-content-center align-items-center">
                <div class="text-center white-text mx-5 wow fadeIn pnt-intro-div">
                    <h1 class="mb-4 pnt-hidden pnt-js-slogan">
                        <strong>Bienvenido a Encuestronic</strong>
                    </h1>
                    <p class = "pnt-hidden pnt-js-slogan-1">Donde las entrevistas</p> <p class="pnt-hidden pnt-js-slogan-2"><strong>VIVEN</strong></p>
                </div>
            </div>
        </div>

        <!--Main layout-->
        <main>
            <div class="fluid-container pnt-hidden pnt-container-principal pnt-js-container-principal">
                <section class="mask mt-5 wow fadeIn pnt-section-nueva-entrevista pnt-js-section-nueva-entrevista">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <!--form agregar nueva entrevista-->
                                <div class="card-body">
                                    <h4 class="card-title">Nueva entrevista</h4>
                                    <hr>
                                    <!--extraer url y metodo a service-->
                                    <form class ="pnt-js-form-nueva-entrevista" onsubmit="return pnt.ui.entrevistas.submitNuevaEntrevista()">
                                        <div class="row md-form">
                                            <div class="col-md-11">
                                                <div class="row">
                                                    <div class="col-md-4 text-center">
                                                        <input id="inputNuevoEntrevistador" class="form-control pnt-js-nueva-entrevista-entrevistador pnt-text-input-material" placeholder="Entrevistador" type="text" name="entrevistador" required>
                                                    </div>
                                                    <div class="col-md-4 text-center">
                                                        <input id="inputNuevoNombre" class="form-control pnt-js-nueva-entrevista-entrevistado pnt-text-input-material" placeholder="Entrevistado" type="text" name="nombre" required>
                                                    </div>
                                                    <div class="col-md-4 text-center">
                                                        <div class="input-group date pnt-js-nueva-entrevista-date-picker" data-entrevista-fecha="Fecha" data-target-input="nearest">
                                                            <input type="text" class="form-control datetimepicker-input pnt-text-input-material pnt-js-readonly-input" data-target=".pnt-js-nueva-entrevista-date-picker" placeholder="Fecha" required/>
                                                            <div class="input-group-append" data-target=".pnt-js-nueva-entrevista-date-picker" data-toggle="datetimepicker">
                                                                <div class="input-group-text pnt-js-show-calendar"><i class="fa fa-calendar"></i></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-10 text-center">
                                                        <input id="inputNuevaDescripcion" class="form-control pnt-js-nueva-entrevista-descripcion pnt-text-input-material" placeholder="Descripción" type="text" name="descripcion">
                                                    </div>
                                                    <div class="col-md-2 form-check pnt-form-check custom-control text-center pnt-checkbox-input-material">
                                                        <input id="inputNuevaRealizada" class="pnt-js-nueva-entrevista-realizada-checkbox" type="checkbox"/>
                                                        <label class="pnt-form-label form-check-label" for="inputNuevaRealizada">Realizada</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-1 my-auto">
                                                <input id="inputSubmit" type="submit" hidden></input>
                                                <label for="inputSubmit" data-toggle="tooltip" title="Crear entrevista" class="pnt-form-label btn btn-md blue darken-2 white-text pnt-button"><i class="fa fa-plus"></i></label>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!--fin form agregar nueva entrevista-->
                            </div>
                        </div>
                    </div>
                </section>
                </br></br>
                <section class="pnt-section-entrevistas pnt-js-section-entrevistas">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Entrevistas</h4>
                                    <hr>
                                    <table class="table table-hover table-responsive-md table-fixed pnt-js-tabla-entrevistas pnt-table-entrevistas">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th>Entrevistador</th>
                                                <th>Nombre Entrevistado</th>
                                                <th>Descripción</th>
                                                <th class="pnt-header-90 text-center">Realizada</th>
                                                <th>Fecha</th>
                                                <th class="text-center">Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody class = "pnt-js-body-tabla-entrevistas">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>

            </div>

        </main>

        <!--Script JS Render-->
        <script id ="templateBodyTablaEntrevistas" type="text/x-jsrender">
            {{for entrevistas}}
            <tr class="pnt-js-entrevista" data-entrevista-id="{{:id}}">
            <td class="pnt-js-entrevista-entrevistador" data-entrevista-id="{{:id}}">{{:entrevistador}}</td>
            <td class="pnt-js-entrevista-entrevistado" data-entrevista-id="{{:id}}">{{:entrevistado}}</td>
            <td class="pnt-js-entrevista-descripcion" data-entrevista-id="{{:id}}">{{:descripcion}}</td>
            <td class="pnt-js-entrevista-realizada text-center">
            {{if realizada}}
            <input class="pnt-js-entrevista-realizada-checkbox" type="checkbox" data-entrevista-id="{{:id}}" checked/>
            {{else}}
            <input class="pnt-js-entrevista-realizada-checkbox" type="checkbox" data-entrevista-id="{{:id}}"/>
            {{/if}}
            <td class="pnt-js-entrevista-fecha" data-entrevista-id="{{:id}}" data-entrevista-fecha="{{fromDateToISOString:fecha}}">{{formatear:fecha}}</td>
            </td>
            <td class="pnt-js-entrevista-acciones text-center">
            <a class="btn btn-md blue darken-2 white-text waves-effect pnt-button pnt-js-modificar-entrevista-boton" data-entrevista-id="{{:id}}" data-toggle="tooltip" title="Modificar" data-modificando=false><i class="fa fa-edit"></i></a>
            <a class="btn btn-md red darken-2 white-text waves-effect pnt-button pnt-js-borrar-entrevista-boton" data-entrevista-id="{{:id}}" data-toggle="tooltip" title="Eliminar"><i class="fa fa-trash"></i></a>
            </td>
            </tr>
            {{/for}}
        </script>
        <!--Script JS Render-->

        <!--Modal de confirmación de borrado-->

        <div class="modal fade" id="modalConfirmacion" tabindex="-1" role="dialog" aria-labelledby="modalConfirmacionLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title pnt-js-modal-confirmacion-titulo" id="modalConfirmacionLabel">Confirmar borrado</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Cancelar">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body pnt-js-modal-confirmacion-body">
                        ...
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-lg green darken-2 white-text waves-effect pnt-js-confirmar-borrado-boton"><i class="fa fa-check"></i></a>
                        <a class="btn btn-lg red darken-2 white-text waves-effect" data-dismiss="modal"><i class="fa fa-times"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <!--Modal de confirmación de borrado-->

        <!-- Lib JS -->
        <!--JQuery-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!--JSRender-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jsrender/0.9.90/jsrender.min.js"></script>
        <!--Toastr js-->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="vendor/mdb/js/popper.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="vendor/mdb/js/bootstrap.min.js"></script>
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="vendor/mdb/js/mdb.min.js"></script>
        <!--bootstrap date picker JavaScript-->
        <script type="text/javascript" src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha18/js/tempusdominus-bootstrap-4.min.js" charset="UTF-8"></script>
        <!-- JS de la app -->
        <script src="js/app.js"></script>
        <script src="js/ui/utils.js" charset="UTF-8"></script>
        <script src="js/ui/utilsEntrevistas.js"></script>
        <script src="js/service/entrevistas.js"></script>
        <script src="js/ui/entrevistas.js"></script>

        <script type="text/javascript">
                                        new WOW().init();
        </script>
    </body>
</html>
