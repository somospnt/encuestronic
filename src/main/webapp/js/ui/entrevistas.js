pnt.ui.entrevistas = (function () {

//imports
    var utils = pnt.ui.utils;
    var utilsEntrevistas = pnt.ui.utilsEntrevistas;
    var service = pnt.service.entrevistas;
    function inicializar() {
        utils.inicializar();
        utilsEntrevistas.inicializar();
    }

    function submitNuevaEntrevista() {
        var entrevista = new Object();
        entrevista.id = null;
        entrevista.entrevistado = $(".pnt-js-nueva-entrevista-entrevistado").val();
        entrevista.entrevistador = $(".pnt-js-nueva-entrevista-entrevistador").val();
        if (utilsEntrevistas.validarCampos(entrevista)) {
            entrevista.descripcion = $(".pnt-js-nueva-entrevista-descripcion").val();
            entrevista.realizada = $(".pnt-js-nueva-entrevista-realizada-checkbox").is(":checked") ? true : false;
            entrevista.fecha = $(".pnt-js-nueva-entrevista-date-picker").data("entrevista-fecha");
            service.crearEntrevista(entrevista)
                    .done(function () {
                        utils.mostrarMensajeExito("Entrevista creada.");
                        utilsEntrevistas.cargarEntrevistas();
                        utilsEntrevistas.clearFormNuevaEntrevista();
                    });
        }

        return false;
    }

    function comenzarEdicion() {
        if (!utilsEntrevistas.modificacionEnCurso()) {
            var id = $(this).data("entrevista-id");
            var botonModificar = $(".pnt-js-modificar-entrevista-boton[data-entrevista-id=" + id + "]");
            var iconoBotonModificar = $(".pnt-js-modificar-entrevista-boton[data-entrevista-id=" + id + "]>i");
            var botonBorrar = $(".pnt-js-borrar-entrevista-boton[data-entrevista-id=" + id + "]");
            var iconoBotonBorrar = $(".pnt-js-borrar-entrevista-boton[data-entrevista-id=" + id + "]>i");
            var row = $(".pnt-js-entrevista[data-entrevista-id=" + id + "]");
            var entrevista = utilsEntrevistas.generarEntrevistaDesdeId(id);
            var fecha = moment($(".pnt-js-entrevista-fecha[data-entrevista-id=" + id + "]").data("entrevista-fecha"), "YYYY-MM-DDTHH:mm:ss");
            console.log("fecha obtenida de td: " + fecha.format());

            $.when(
                    row.find(".pnt-js-entrevista-entrevistado[data-entrevista-id=" + id + "]").html('<div class = "md-form pnt-textbox-modificacion pnt-no-margin pnt-no-padding"><input id="inputEntrevistado" class="form-control form-control-sm pnt-textbox-modificacion pnt-js-modificar-entrevistado pnt-no-margin pnt-no-padding pnt-textbox-inline" type="text" name="entrevistado" data-entrevista-id="' + id + '"></div>')
                    ).then($(".pnt-js-modificar-entrevistado[data-entrevista-id=" + id + "]").val(entrevista.entrevistado));
            $.when(
                    row.find(".pnt-js-entrevista-entrevistador[data-entrevista-id=" + id + "]").html('<div class = "md-form pnt-textbox-modificacion pnt-no-margin pnt-no-padding"><input id="inputEntrevistador" class="form-control form-control-sm pnt-textbox-modificacion pnt-js-modificar-entrevistador pnt-no-margin pnt-no-padding pnt-textbox-inline" type="text" name="entrevistador" data-entrevista-id="' + id + '"></div>')
                    ).then($(".pnt-js-modificar-entrevistador[data-entrevista-id=" + id + "]").val(entrevista.entrevistador));
            $.when(
                    row.find(".pnt-js-entrevista-descripcion[data-entrevista-id=" + id + "]").html('<div class = "md-form pnt-textbox-modificacion pnt-no-margin pnt-no-padding"><input id="inputDescripcion" class="form-control form-control-sm pnt-textbox-modificacion pnt-js-modificar-descripcion pnt-no-margin pnt-no-padding pnt-textbox-inline" type="text" name="descripcion" data-entrevista-id="' + id + '"></div>')
                    ).then($(".pnt-js-modificar-descripcion[data-entrevista-id=" + id + "]").val(entrevista.descripcion));
            $.when(row.find(".pnt-js-entrevista-fecha[data-entrevista-id=" + id + "]").html('<div class="md-form input-group date pnt-js-entrevista-date-picker pnt-no-margin pnt-no-padding" data-entrevista-fecha="Fecha"  data-entrevista-id=\'' + id + '\' data-target-input="nearest"> <input type="text" class="form-control-sm datetimepicker-input pnt-js-show-calendar-modificar pnt-no-margin pnt-no-padding" data-target=".pnt-js-entrevista-date-picker[data-entrevista-id=\'' + id + '\'] " data-toggle="datetimepicker" readonly="readonly" /></div></div>')
                    ).then(function () {
                var datePicker = $('.pnt-js-entrevista-date-picker[data-entrevista-id="' + id + '"]');
                utils.bindearDatePicker(datePicker, function () {
                    $('.pnt-js-show-calendar-modificar').on('click', function () {
                        var sectionNuevaEntrevista = $('.pnt-js-section-nueva-entrevista');
                        var sectionEntrevistas = $('.pnt-js-section-entrevistas');
                        if (sectionNuevaEntrevista.css('z-index') > sectionEntrevistas.css('z-index')) {
                            sectionEntrevistas.css('z-index', '3');
                            sectionNuevaEntrevista.css('z-index', '1');
                        }
                    });
                });
                console.log("Fecha " + fecha.format());
                datePicker.datetimepicker('date', fecha);
            });

            botonModificar.data("modificando", true);
            botonModificar.off();
            botonModificar.on("click", aceptarCambios);
            botonBorrar.off();
            botonBorrar.on("click", cancelarEdicion);
            $(".pnt-js-nueva-entrevista-realizada-checkbox[data-entrevista-id=" + id + "]").off();
            iconoBotonBorrar.removeClass("fa-trash");
            iconoBotonBorrar.addClass("fa-times");
            botonBorrar.attr("title", "Cancelar");
            iconoBotonModificar.removeClass("fa-edit");
            botonModificar.removeClass("blue");
            botonModificar.attr("title", "Aceptar");
            iconoBotonModificar.addClass("fa-check");
            botonModificar.addClass("green");
        } else {
            utils.mostrarMensajeInfo("Primero debe terminar la modificaci&oacute;n en curso.");
        }
    }

    function aceptarCambios() {
        var id = $(this).data("entrevista-id");
        var entrevista = {};
        entrevista.id = id;
        entrevista.entrevistado = $(".pnt-js-modificar-entrevistado[data-entrevista-id=" + id + "]").val();
        entrevista.entrevistador = $(".pnt-js-modificar-entrevistador[data-entrevista-id=" + id + "]").val();
        if (utilsEntrevistas.validarCampos(entrevista)) {

            entrevista.descripcion = $(".pnt-js-modificar-descripcion[data-entrevista-id=" + id + "]").val();
            entrevista.realizada = $(".pnt-js-entrevista-realizada-checkbox[data-entrevista-id=" + id + "]").is(":checked") ? true : false;
            console.log(entrevista.fecha);
            entrevista.fecha = $(".pnt-js-entrevista-date-picker[data-entrevista-id=" + id + "]").data("entrevista-fecha");
            console.log(entrevista.fecha);
            console.log("JSON: " + JSON.stringify(entrevista));
            service.modificarEntrevista(entrevista).done(function () {
                utils.mostrarMensajeExito("Entrevista modificada.");
                finalizarEdicion();
                utilsEntrevistas.cargarEntrevistas();
            });
        }
    }

    function cancelarEdicion() {
        utils.mostrarMensajeInfo("Edici&oacute;n cancelada.");
        finalizarEdicion();
    }

    function finalizarEdicion() {
        utilsEntrevistas.cargarEntrevistas();
    }

    return {
        inicializar: inicializar,
        submitNuevaEntrevista: submitNuevaEntrevista,
        comenzarEdicion: comenzarEdicion,
    };
})();
$(document).ready(function () {
    pnt.ui.entrevistas.inicializar();
});


