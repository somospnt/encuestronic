pnt.ui.utilsEntrevistas = (function () {

    //imports
    var utils = pnt.ui.utils;
    function inicializar() {
        cargarEntrevistas();
        bindearDatePickerNuevaEntrevista();
        bindearInputReadOnly();
    }

    function bindearDatePickerNuevaEntrevista() {
        utils.bindearDatePicker('.pnt-js-nueva-entrevista-date-picker',
                function () {
                    $('.pnt-js-show-calendar').on('click', function () {
                        var sectionNuevaEntrevista = $('.pnt-js-section-nueva-entrevista');
                        var sectionEntrevistas = $('.pnt-js-section-entrevistas');

                        if (sectionNuevaEntrevista.css('z-index') < sectionEntrevistas.css('z-index')) {
                            sectionEntrevistas.css('z-index', '1');
                            sectionNuevaEntrevista.css('z-index', '3');
                        }

                    });
                }
        );
    }

    function bindearInputReadOnly() {
        $(".pnt-js-readonly-input").on('keydown paste', function (e) {
            e.preventDefault();
        });
    }

    function cargarEntrevistas() {
        pnt.service.entrevistas.buscarTodas()
                .done(function (entrevistas) {
                    for (entrevista of entrevistas) {
                        entrevista.fecha = utils.parsearFecha(entrevista.fecha);
                    }
                    let data = {entrevistas: entrevistas};
                    utils.renderizarTemplate("#templateBodyTablaEntrevistas", ".pnt-js-body-tabla-entrevistas", data, bindearElementosTabla);
                })
                .fail(function () {
                    utils.mostrarMensajeError("Fallo al cargar entrevistas de la base de datos.");
                });
    }

    function bindearElementosTabla() {
        bindearCheckboxRealizadas();
        bindearBotonModificarEntrevista();
        bindearBotonBorrarEntrevista();
    }

    function bindearBotonModificarEntrevista() {
        $(".pnt-js-modificar-entrevista-boton").on("click", pnt.ui.entrevistas.comenzarEdicion);
    }

    function bindearBotonBorrarEntrevista() {
        $(".pnt-js-borrar-entrevista-boton").on("click", function () {
            if (!modificacionEnCurso()) {
                var id = $(this).data("entrevista-id");
                var entrevista = generarEntrevistaDesdeId(id);

                $(".pnt-js-modal-confirmacion-body").html(generarContenidoModalDeConfirmacion(entrevista));

                abrirModalConfirmacion();

                $(".pnt-js-confirmar-borrado-boton").off();
                $(".pnt-js-confirmar-borrado-boton").on("click", function () {
                    pnt.service.entrevistas.borrarEntrevista(entrevista).done(function () {
                        utils.mostrarMensajeExito("Entrevista borrada.");
                        cargarEntrevistas();
                        $("#modalConfirmacion").modal("hide");
                    });

                });
            } else {
                utils.mostrarMensajeInfo("Primero debe terminar la modificaci&oacute;n en curso.");
            }
        });
    }

    function bindearCheckboxRealizadas() {
        $(".pnt-js-entrevista-realizada-checkbox").on("click", function () {
            var entrevistaId = $(this).data("entrevista-id");
            if ($(".pnt-js-modificar-entrevista-boton[data-entrevista-id=" + entrevistaId + "]").data("modificando") === false) {
                var entrevista = generarEntrevistaDesdeId(entrevistaId);
                pnt.service.entrevistas.modificarEntrevista(entrevista)
                        .done(function () {
                            if (entrevista.realizada) {
                                utils.mostrarMensajeExito("Entrevista marcada como <strong>realizada</strong>.");
                            } else {
                                utils.mostrarMensajeExito("Entrevista marcada como <strong>no realizada</strong>.");
                            }
                        })
                        .fail(function () {
                            utils.mostrarMensajeError("No se pudo cambiar el estado de la entrevista.");
                        });
            }
        });
    }

    function generarContenidoModalDeConfirmacion(entrevista) {
        return "<p>&iquest;Seguro que quiere borrar la entrevista con <strong>"
                + entrevista.entrevistado
                + "</strong>?"
                + "</p>";
    }

    function generarEntrevistaDesdeId(id) {
        var rowEncontrada;
        $(".pnt-js-entrevista").each(function () {
            var row = $(this);
            if (row.data("entrevista-id") === id) {
                rowEncontrada = row;
            }
        });

        var entrevista = {};
        entrevista.id = rowEncontrada.data("entrevista-id");
        entrevista.entrevistado = rowEncontrada.find(".pnt-js-entrevista-entrevistado").html();
        entrevista.entrevistador = rowEncontrada.find(".pnt-js-entrevista-entrevistador").html();
        entrevista.descripcion = rowEncontrada.find(".pnt-js-entrevista-descripcion").html();
        if (rowEncontrada.find(".pnt-js-entrevista-realizada").find(".pnt-js-entrevista-realizada-checkbox").is(":checked")) {
            entrevista.realizada = true;
        } else {
            entrevista.realizada = false;
        }
        entrevista.fecha = utils.parsearFecha(rowEncontrada.find(".pnt-js-entrevista-fecha").data("entrevista-fecha"));
        return entrevista;
    }

    function clearFormNuevaEntrevista() {
        $(".pnt-js-nueva-entrevista-entrevistado").val("");
        $(".pnt-js-nueva-entrevista-entrevistador").val("");
        $(".pnt-js-nueva-entrevista-descripcion").val("");
        $(".pnt-js-nueva-entrevista-realizada-checkbox").prop('checked', false);
        $(".pnt-js-nueva-entrevista-date-picker").datetimepicker('clear');

    }

    function modificacionEnCurso() {
        var botonesModificar = $(".pnt-js-modificar-entrevista-boton");
        for (i = 0; i < botonesModificar.length; i++) {
            var boton = botonesModificar[i];
            if ($(boton).data("modificando") === true) {
                return true;
            }
        }
        return false;
    }

    function validarCampos(entrevista) {
        valido = true;
        var mensajeValidacion = "<h5>Errores de validaci&oacute;n:</h5>";

        if (entrevista.entrevistado.trim() === "") {
            mensajeValidacion += "Debe escribir un nombre de entrevistado";
            $(".pnt-js-nueva-entrevista-entrevistado").val("");
            $(".pnt-js-nueva-entrevista-entrevistado").focus();
            valido = false;
        }

        if (entrevista.entrevistador.trim() === "") {
            mensajeValidacion += mensajeValidacion.includes("Debe escribir un nombre de entrevistado") ? " y un nombre de entrevistador" : "Debe escribir un nombre de entrevistador";
            $(".pnt-js-nueva-entrevista-entrevistador").val("");
            $(".pnt-js-nueva-entrevista-entrevistador").focus();
            valido = false;
        }

        if (valido) {
            return true;
        } else {
            utils.mostrarMensajeError(mensajeValidacion.trim());
            return false;
        }
    }

    function abrirModalConfirmacion() {
        $("#modalConfirmacion").modal("show");
    }

    return {
        validarCampos: validarCampos,
        clearFormNuevaEntrevista: clearFormNuevaEntrevista,
        cargarEntrevistas: cargarEntrevistas,
        generarEntrevistaDesdeId: generarEntrevistaDesdeId,
        modificacionEnCurso: modificacionEnCurso,
        inicializar: inicializar
    };

})();


