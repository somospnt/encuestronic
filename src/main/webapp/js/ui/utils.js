pnt.ui.utils = (function () {

    function inicializar() {
        addJSRenderComponents();
        animacionInicial();
        inicializarToastr();
        inicializarTooltip();
    }

    function inicializarTooltip() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    function addJSRenderComponents() {
        $.views.converters("formatear", function (fecha) {
            fecha.locale('es');
            stringFecha = capitalizeFirstLetter(fecha.format("LLL"));
            return stringFecha;
        });

        $.views.converters("fromDateToISOString", function (fecha) {
            return toISOString(fecha);
        });
    }

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function inicializarToastr() {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": 300,
            "hideDuration": 300,
            "timeOut": 3000,
            "extendedTimeOut": 1000,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    }

    function mostrarMensajeError(mensaje) {
        toastr.error(mensaje);
    }

    function mostrarMensajeExito(mensaje) {
        toastr.success(mensaje);
    }

    function mostrarMensajeInfo(mensaje) {
        toastr.info(mensaje);
    }

    function renderizarTemplate(selectorTemplate, selector, datos, callback) {
        var tablaEntrevistasTemplate;
        tablaEntrevistasTemplate = $.templates($(selectorTemplate).html());
        var html = tablaEntrevistasTemplate.render(datos);
        $(selector).html(html);
        callback();
    }

    function bindearDatePicker(selector, callback) {
        var datePicker = $(selector);

        datePicker.datetimepicker({
            format: "LLL",
            locale: "es",
            ignoreReadonly: true,
            allowInputToggle: true,
            sideBySide: true,
            buttons: {
                showToday: true,
                showClose: true
            },
            tooltips: {
                today: "Ir a la fecha de hoy",
                close: 'Cerrar',
                selectMonth: 'Seleccionar mes',
                prevMonth: 'Mes anterior',
                nextMonth: 'Mes siguiente',
                selectYear: 'Seleccionar año',
                prevYear: 'Año anterior',
                nextYear: 'Año siguiente',
                selectDecade: 'Seleccionar década',
                prevDecade: 'Década anterior',
                nextDecade: 'Década siguiente',
                prevCentury: 'Siglo anterior',
                nextCentury: 'Siglo siguiente',
                incrementHour: 'Aumentar hora',
                pickHour: 'Seleccionar hora',
                decrementHour: 'Disminuir hora',
                incrementMinute: 'Aumentar minutos',
                pickMinute: 'Seleccionar minutos',
                decrementMinute: 'Disminuir minutos',
                incrementSecond: 'Aumentar segundos',
                pickSecond: 'Seleccionar segundos',
                decrementSecond: 'Disminuir segundos',
                selectTime: 'Seleccionar hora',
                selectDate: 'Seleccionar fecha'
            }

        });

        datePicker.on('change.datetimepicker', function () {
            var date = datePicker.datetimepicker('viewDate');
            if (date) {
                datePicker.data("entrevista-fecha", date.format('YYYY-MM-DDTHH:mm:ss'));
            } else {
                console.log('No se pudo obtener el date');
            }
        });
        callback();
    }

    function animacionInicial() {
        $(window).scrollTop(0);
        $(".pnt-js-slogan").delay(300).fadeIn(800, function () {
            $(".pnt-js-slogan-1").delay(500).fadeIn(800, function () {
                $(".pnt-js-slogan-2").delay(200).fadeIn(800);
            });
        });
        $(".pnt-js-intro").delay(4000).fadeOut(500, function () {
            $(".pnt-js-container-principal").fadeIn(500, null);
            $(".pnt-js-navbar").fadeIn(500, null);
        });
    }

    function parsearFecha(stringFecha) {
        return moment(stringFecha, 'YYYY-MM-DDTHH:mm:ss');
    }

    function toISOString(date) {
        return date.format('YYYY-MM-DDTHH:mm:ss');
    }

    return {
        renderizarTemplate: renderizarTemplate,
        mostrarMensajeExito: mostrarMensajeExito,
        mostrarMensajeError: mostrarMensajeError,
        mostrarMensajeInfo: mostrarMensajeInfo,
        parsearFecha: parsearFecha,
        inicializar: inicializar,
        toISOString: toISOString,
        bindearDatePicker: bindearDatePicker
    };
})();
