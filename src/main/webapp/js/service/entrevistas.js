pnt.service.entrevistas = (function () {

    var urlService = "/encuestronic/api/entrevistas/";

    function modificarEntrevista(entrevista) {
        var urlServiceBuscarPorId = urlService + entrevista.id;
        return $.ajax({
            url: urlServiceBuscarPorId,
            type: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(entrevista)
        });
    }

    function crearEntrevista(entrevista) {
        return $.ajax({
            url: urlService,
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(entrevista)
        });
    }

    function buscarTodas() {
        return $.ajax({
            url: urlService,
            type: 'GET'
        });
    }

    function borrarEntrevista(entrevista) {
        var urlServiceBorrar = urlService + entrevista.id;
        return $.ajax({
            url: urlServiceBorrar,
            type: 'DELETE',
            contentType: 'application/json',
            data: JSON.stringify(entrevista)
        });
    }

    return{
        modificarEntrevista: modificarEntrevista,
        crearEntrevista: crearEntrevista,
        buscarTodas: buscarTodas,
        borrarEntrevista: borrarEntrevista
    };
}
)();