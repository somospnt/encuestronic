package com.connectis.entrevistas;

import com.connectis.encuestronic.EntrevistasApplication;
import javax.transaction.Transactional;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(classes = EntrevistasApplication.class)
public abstract class EntrevistasApplicationTests {

}
