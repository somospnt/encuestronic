package com.connectis.entrevistas.service;

import com.connectis.encuestronic.domain.Entrevista;
import com.connectis.encuestronic.repository.EntrevistaRepository;
import com.connectis.encuestronic.service.EntrevistaService;
import com.connectis.entrevistas.EntrevistasApplicationTests;
import java.time.LocalDateTime;
import java.time.Month;
import static org.assertj.core.api.Assertions.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class EntrevistaServiceTest extends EntrevistasApplicationTests {

    @Autowired
    private EntrevistaService entrevistaService;
    @Autowired
    private EntrevistaRepository entrevistaRepository;

    @Test
    public void buscarTodas_sinParametros_retornaListaDeEntrevistas() {
        assertThat(entrevistaService.buscarTodas()).isNotNull().hasSize(4);
    }

    @Test
    public void modificarEntrevista_conEntrevistaExistente_modificaEntrevista() {
        final String entrevistado = "otro entrevistado";
        final String entrevistador = "otro entrevistador";
        final String descripcion = "otra descripcion";
        final LocalDateTime fecha = LocalDateTime.of(2018, Month.APRIL, 20, 9, 0);
        final Boolean realizada = false;

        Entrevista entrevista = entrevistaRepository.findById(1L).get();

        entrevista.setEntrevistado(entrevistado);
        entrevista.setEntrevistador(entrevistador);
        entrevista.setDescripcion(descripcion);
        entrevista.setFecha(fecha);
        entrevista.setRealizada(realizada);
        entrevistaService.modificarEntrevista(entrevista);

        Entrevista entrevistaModificada = entrevistaRepository.findById(1L).get();

        assertThat(entrevista.getEntrevistado()).isEqualTo(entrevistaModificada.getEntrevistado());
        assertThat(entrevista.getEntrevistador()).isEqualTo(entrevistaModificada.getEntrevistador());
        assertThat(entrevista.getDescripcion()).isEqualTo(entrevistaModificada.getDescripcion());
        assertThat(entrevista.getFecha()).isEqualTo(entrevistaModificada.getFecha());
        assertThat(entrevista.getRealizada()).isEqualTo(entrevistaModificada.getRealizada());
    }

    @Test(expected = IllegalArgumentException.class)
    public void modificarEntrevista_conEntrevistaInexistenteIdNulo_lanzaExcepcion() {
        entrevistaService.modificarEntrevista(new Entrevista());
    }

    @Test(expected = IllegalArgumentException.class)
    public void modificarEntrevista_conEntrevistaInexistenteIdFueraDeRango_lanzaExcepcion() {
        entrevistaService.modificarEntrevista(Entrevista.builder().id(45L).build());
    }

    @Test(expected = IllegalArgumentException.class)
    public void modificarEntrevista_conNulo_lanzaExcepcion() {
        entrevistaService.modificarEntrevista(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void modificarEntrevista_conEntrevistaExistenteNombreEntrevistadoNulo_lanzaExcepcion() {
        Entrevista entrevista = entrevistaRepository.findById(1L).get();
        entrevista.setEntrevistado(null);
        entrevistaService.modificarEntrevista(entrevista);
    }

    @Test(expected = IllegalArgumentException.class)
    public void modificarEntrevista_conEntrevistaExistenteNombreEntrevistadoVacio_lanzaExcepcion() {
        Entrevista entrevista = entrevistaRepository.findById(1L).get();
        entrevista.setEntrevistado("");
        entrevistaService.modificarEntrevista(entrevista);
    }

    @Test(expected = IllegalArgumentException.class)
    public void modificarEntrevista_conEntrevistaExistenteNombreEntrevistadorNulo_lanzaExcepcion() {
        Entrevista entrevista = entrevistaRepository.findById(1L).get();
        entrevista.setEntrevistador(null);
        entrevistaService.modificarEntrevista(entrevista);
    }

    @Test(expected = IllegalArgumentException.class)
    public void modificarEntrevista_conEntrevistaExistenteNombreEntrevistadorVacio_lanzaExcepcion() {
        Entrevista entrevista = entrevistaRepository.findById(1L).get();
        entrevista.setEntrevistador("");
        entrevistaService.modificarEntrevista(entrevista);
    }

    @Test
    public void crearEntrevista_conEntrevistaNuevaSinId_creaEntrevista() {
        Entrevista entrevistaCreada = entrevistaService.crearEntrevista(Entrevista.builder().entrevistado("nombre").build());
        assertThat(entrevistaCreada).isNotNull();
        assertThat(entrevistaCreada.getId()).isNotNull();
    }

    @Test(expected = IllegalArgumentException.class)
    public void crearEntrevista_conEntrevistaNula_lanzaExcepcion() {
        entrevistaService.crearEntrevista(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void crearEntrevista_conEntrevistaNuevaConIdExistente_lanzaExcepcion() {
        entrevistaService.crearEntrevista(entrevistaRepository.findById(1L).get());
    }

    @Test(expected = IllegalArgumentException.class)
    public void crearEntrevista_conEntrevistaNuevaConNombreEntrevistadoVacio_lanzaExcepcion() {
        entrevistaService.crearEntrevista(Entrevista.builder().entrevistado("").build());
    }

    @Test(expected = IllegalArgumentException.class)
    public void crearEntrevista_conEntrevistaNuevaConNombreEntrevistadoSoloEspacios_lanzaExcepcion() {
        entrevistaService.crearEntrevista(Entrevista.builder().entrevistado("   ").build());
    }

    @Test
    public void borrarEntrevista_conEntrevistaExistente_borraEntrevista() {

        Entrevista entrevista = entrevistaRepository.findById(1L).get();
        entrevistaService.borrarEntrevista(entrevista);
        assertThat(entrevistaRepository.findById(1L)).isEmpty();
    }

    @Test(expected = IllegalArgumentException.class)
    public void borrarEntrevista_conEntrevistaInexistente_lanzaExcepcion() {
        entrevistaService.borrarEntrevista(Entrevista.builder().id(66L).entrevistado("Pepito").descripcion("Nada").realizada(false).build());
    }

    @Test(expected = IllegalArgumentException.class)
    public void borrarEntrevista_conEntrevistaNula_lanzaExcepcion() {
        entrevistaService.borrarEntrevista(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void borrarEntrevista_conEntrevistaConIdNulo_lanzaExcepcion() {
        Entrevista entrevista = entrevistaRepository.findById(1L).get();
        entrevista.setId(null);
        entrevistaService.borrarEntrevista(entrevista);
    }

}
