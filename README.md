﻿##Proyecto Encuestronic

## Descripción:  
Aplicación web para seguimiento de entrevistas realizadas en PNT.  

## Requisitos:  
* Java 8  
* Maven  

## Cómo correrlo:  
TODO  

## Dónde empezar:
Una vez levantando el server entrá http://localhost:8080/encuestronic/ y seguí las instrucciones.  

## API REST    

### /api/entrevistas

```javascript   
GET /api/entrevistas/			RESPONSE: List<Entrevista>  

POST /api/entrevistas/		@RequestBody Entrevista entrevista  
								RESPONSE: Entrevista  

PUT /api/entrevistas/{id}	@PathVariable Long id,  
							@RequestBody Entrevista entrevista  
								RESPONSE: Entrevista   
```

## Documentación    

* [Diagrama ER](https://bitbucket.org/robergzt/entrevista-app/src/master/doc/diagramas/)  
* [Diagramas de dominio y clase](https://drive.google.com/file/d/1Jq-MI2sM9lR1u9Quq4Z2eAu0E7qp2Grw/view?usp=sharing)  
* [DDT](https://drive.google.com/open?id=1odXD0_WXgiPzzkgPAvRbhs5o8grvbP-2)